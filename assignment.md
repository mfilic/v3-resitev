# Node.js in Pug V3

Za lažje razumevanje vaj si poglejte vsebino predavanj <span class="sklop2">P2.2</span> [Uporaba JavaScript na strani odjemalca (brskalnik) in strežnika (Node.js)](#JavaScript-brskalnik-Node-js) in <span class="sklop3">P3.1</span> [MVC pristop (Express ogrodje) in uporaba HTML predlog (JADE) v MEAN arhitekturi](#MVC-MEAN).

## Uvod
V okviru te naloge želimo implementirati odjemalsko aplikacijo z Node.js. 
Za osnovo lahko uporabite repozitorij [<img src="img/git.png" class="git">V3](https://bitbucket.org/spfri/v3). Najbolje, da naredite svojo kopijo repozitorija (angl. fork),
kjer izvedete naslednje ukaze:

~~~~ {.bash}
git clone https://{študent}@bitbucket.org/spfri/v3.git
cd v3
~~~~

V nadaljevanju se predpostavlja, da delate s kodo iz zgornjega repozitorija.

Aplikacija omogoča komentiranje in je sestavljena iz naslednjih elementov:

* **index** stran, kjer administrator objavlja obvestila,
* **comments** stran, ki vsem omogoča dodajanje komentarjev,
* navigacijska vrstica z iskanjem.

Za dodajanje komentarjev moramo obiskati stran **comments**, kjer lahko pregledamo obstoječe komentarje in novega vnesemo z izpolnitvijo polj `Name`, `Comment` ter po želji izberemo sliko, ki se bo pojavila skupaj s komentarjem.

V kodi pripravljenega repozitorija imate dve HTML datoteki, ki jih bomo pretvorili v PUG predlogi in prilagodili aplikacijo MVC arhitekturi. Jezik JavaScript bomo uporabili za preverjanje podatkov na ravni odjemalca, t.j. podatke obrazca za komentarje.

Rešitev teh vaj bo predstavljala začetni repozitorij za naslednje laboratorijske vaje. Tisti, ki ste dovolj pogumni, lahko začnete tudi z izdelavo podpore podatkovni bazi.

## Priprava okolja

1. Pred začetkom se prepričajmo, da imamo nameščeni orodji *node* in *npm*. To lahko preverimo z ukazoma `node -v` in `npm -v`. Če se nam izpišejo verzije obeh orodij, potem sta ustrezno nameščeni. Če še nimamo nameščenega orodja *express-generator*, ga namestimo. Na voljo imamo dve možnosti:

    * v lokalno v mapo, kjer se nahajamo:
	    * `npm install express-generator` in ga nato lahko uporabimo sledeče:
	    * `./node_modules/express-generator/bin/express-cli.js --view=pug IME_PROJEKTA`
    * globalno, da bo dostopen v celotnem sistemu:
	    * `npm install -g express-generator` in ga nato uporabljamo sledeče:
	    * `express --view=pug IME_PROJEKTA`

    Stikalo `--view=pug` uporabimo zato, da bodo predloge (angl. templates) v trenutno najnovejšem formatu, ki ga priporočajo razvijalci express. Če izpustimo parameter `IME_PROJEKTA`, potem se nam projekt ustvari v trenutni mapi.

2. Ustvaril se vam je projekt z osnovnimi datotekami. Najprej prenesimo vse potrebne knjižnice z ukazom `npm install` in ga nato zaženimo z `npm run start`. 

    Če uporabljate okolje Cloud9, potem se vaša aplikacija nahaja na: `https://DELOVNO\_OKOLJE-UPORABNISKO\_IME.c9users.io/`.
    
3. Sedaj vključite v uveljavitev za git vse datoteke v projektu. Pri tem ne želite vključiti map, ki se ustvarijo, ko aplikacijo prevedete ali map, ki so le produkt zagona aplikacije.

    Zaradi tega ustvarite datoteko `.gitignore` in vanjo dodajte vrstici `node_modules/` in `npm-debug.log`.
    
4. Ustvarite uveljavitev in nato nadaljujte z delom.

## Prenos in priprava pogledov PUG

1. Iz statične različice aplikacije skopiramo `.js` in `.css` datoteke. JavaScript datoteke skopiramo v mapo `public/javascript`, slike v mapo `public/images` in stile v mapo `public/stylesheets`.

2. HTML datoteki (ročno) pretvorimo v PUG predlogi. 

    Pri tem si lahko pomagate z avtomatskimi pretvorniki, ki so dostopni na spletu - [HTML2PUG](https://html2pug.herokuapp.com/), [HTML2JADE](http://html2jade.vida.io/) ali npm modulom [HTML2JADE](https://www.npmjs.com/package/html2jade).


##Sprememba strukture mape za MVC aplikacijo
Spremenimo strukturo mape za MVC aplikacijo, kakor je opisano na predavanjih <span class="sklop3">P3.1</span> [MVC pristop (Express ogrodje) in uporaba HTML predlog (PUG) v MEAN arhitekturi](#prilagoditev-ogrodja-express-za-mvc)

Opazili ste, da spletni strani ne izgledata enako oz. jima manjkajo slike in stili. Ustrezno popravite pot do teh datotek, ki se sedaj nahajajo v mapah `/images`, `/stylesheets` in `/javascripts`. 

Ko vam obe prenešeni strani ustrezno delujeta, uveljavite spremembe.

## Določite različne krmilnike za različne sklope funkcionalnosti

Za našo aplikacijo je smiselno imeti dva različna krmilnika:

* komentarji,
* ostalo.

Eden je za prikaz komentarja in njegovo dodajanje, drugi pa za vse ostale funkcionalnosti (prikaz admin komentarja na index strani).

## Premik podatkov iz pogledov v krmilnike

Dinamično vsebino aplikacije predstavljajo komentarji, ki imajo naslednjo strukturo,

~~~~ {.json .numberLines startFrom="1"}
{ 
  "name": String, 
  "comment": String,
  "image": String
}
~~~~

kjer `image` predstavlja pot do določene slika, če jo je uporabnik shranil skupaj s komentarjem.

Te dinamične vsebine se nahajajo v predlogah PUG, ki jih zahtevamo preko krmilnikov v Node.js aplikaciji. Če spletna stran prikaže seznam komentarjev, morajo biti ti komentarji prikazani s pomočjo predloge PUG, podatki pa so pridobljeni iz krmilnika, kot je bilo predstavljeno na predavanju.

Podatke, ki se uporabljajo v krmilniku, shranite v mapo modela in za to uporabite JSON format. Omenjene podatke (v krmilniku) preberite po potrebi.

## Preverite podatke na ravni odjemalca

Po kliku za objavo komentarja se naj izvede lastna JavaScript funkcija, katere koda se nahaja v zasebni `.js` datoteki in ne v `.html` datoteki. JavaScript funkcija mora preveriti ali so vneseni podatki ustrezni:

* v imenu uporabnika ni ločil oz. znakov, ki niso alfanumerični,
* komentarji niso daljši od 500 znakov.

V primeru napake uporabnika obvestimo z opisom napake.

## Posebne PUG predloge: layout in mixin

Če tega še niste implementirali, potem PUG predloge preuredite tako, da uporabite vsaj en element `layout` in `mixin` pri prikazu uporabniških komentarjev.

> Sedaj si lahko čestitate in greste na sprehod.